# Dark Souls III AutoSplit
A LiveSplit auto splitter component, supporting Ingame Time.

Currently supported game versions:
- 1.12
- 1.15

Currently supported categories:
- All Bosses (No DLC)

## Installation

- Get the latest [LiveSplit](https://livesplit.org) version
- Download the ZIP archive from [latest release](https://gitlab.com/dwonisch/ds3autosplit/-/releases)
- Extract archive content into '[LiveSplit]\Components' folder.
- Start LiveSplit. Right click > Edit Layout.
- In Layout Editor press the +-Button. Select Timer > Dark Souls III AutoSplit. Press OK
- In LiveSplit. Right click > Edit Splits. Select Dark Souls III and desired category.

## Features

- If you haven't created splits already. You can use 'Create Splits'-Button in Splitter Settings.