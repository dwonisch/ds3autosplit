﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DS3AutoSplit.Settings;
using Memory;

namespace DS3AutoSplit.Splits {
    public interface ISplit {
        bool Completed { get; }
        long GameTimeCompleted { get; }
        bool Split(long gameTime, ProcessMemory memory);
        void UndoSplit();
    }
}
