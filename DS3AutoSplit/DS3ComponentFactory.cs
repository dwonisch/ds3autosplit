﻿using System;
using System.Reflection;
using DS3AutoSplit;
using LiveSplit.Model;
using LiveSplit.UI.Components;

//Register component factory, to be found by LiveSplit
[assembly: ComponentFactory(typeof(Ds3ComponentFactory))]

namespace DS3AutoSplit {
    public class Ds3ComponentFactory : IComponentFactory {
        public string UpdateName => ComponentName;
        public string XMLURL => "https://gitlab.com/dwonisch/ds3autosplit/-/raw/master/Components/update.Ds3AutoSplit.xml";
        public string UpdateURL => "https://gitlab.com/dwonisch/ds3autosplit/-/raw/master/Components/DS3AutoSplit.dll";
        public Version Version => Assembly.GetExecutingAssembly().GetName().Version;
        public IComponent Create(LiveSplitState state) {
            return new Ds3Component(state);
        }

        public string ComponentName => "Dark Souls III AutoSplit";
        public string Description => "Dark Souls III - AutoSplit and InGame Timer";
        public ComponentCategory Category => ComponentCategory.Timer;
    }
}
