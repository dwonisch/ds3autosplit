﻿using System;
using System.Diagnostics;
using DS3AutoSplit.Settings;
using Memory;

namespace DS3AutoSplit.Splits {
    
    public class WorldFlagSplit : ISplit {
        private readonly WorldFlagSplitSetting setting;
        private readonly IntPtr worldFlagOffset;

        public WorldFlagSplit(WorldFlagSplitSetting setting, IntPtr worldFlagOffset) {
            this.setting = setting;
            this.worldFlagOffset = worldFlagOffset;
        }

        public bool Completed { get; private set; }
        public long GameTimeCompleted { get; private set; }
        
        public bool Split(long gameTime, ProcessMemory memory) {
            if (!setting.Active)
                return false;

            if (Completed)
                return false;

            var settingOffset = worldFlagOffset + setting.Offset;
            var readByte = memory.ReadInt32(settingOffset);
            Completed = Memory.Memory.GetFlag(readByte, setting.Flag);

            if (Completed) {
                GameTimeCompleted = gameTime;
                Trace.WriteLine($"Checking {setting.Name} - {settingOffset} - {readByte} - {setting.Flag} - {Completed}");
            }

            return Completed;
        }

        public void UndoSplit() {
            Completed = false;
            GameTimeCompleted = 0;
        }
    }
}