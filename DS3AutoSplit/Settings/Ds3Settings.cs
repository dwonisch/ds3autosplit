﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Windows.Forms;
using System.Xml;
using DS3AutoSplit.Splits;
using LiveSplit.Model;
using LiveSplit.UI;

namespace DS3AutoSplit.Settings {
    public class Ds3Settings {
        private readonly LiveSplitState liveSplitState;
        public event EventHandler SettingsUpdated;

        public Ds3Settings(LiveSplitState liveSplitState) {
            this.liveSplitState = liveSplitState;
            WorldFlagSplits = new List<WorldFlagSplitSetting>();

            WorldFlagSplits.Add(new WorldFlagSplitSetting("Iudex Gundyr", 0x5A67, 7));
            WorldFlagSplits.Add(new WorldFlagSplitSetting("Curse-Rotted Greatwood", 0x1967, 7));
            WorldFlagSplits.Add(new WorldFlagSplitSetting("Vordt of the Boreal Valley", 0xF67, 7));
            WorldFlagSplits.Add(new WorldFlagSplitSetting("Abyss Watchers", 0x2D67, 7));
            WorldFlagSplits.Add(new WorldFlagSplitSetting("High Lord Wolnir", 0x5067, 5));
            WorldFlagSplits.Add(new WorldFlagSplitSetting("Crystal Sage", 0x2D69, 5));
            WorldFlagSplits.Add(new WorldFlagSplitSetting("Deacons of the Deep", 0x3C67, 7));
            WorldFlagSplits.Add(new WorldFlagSplitSetting("Pontiff Sulyvahn", 0x4B69, 5));
            WorldFlagSplits.Add(new WorldFlagSplitSetting("Aldrich, Devourer of Gods", 0x4B67, 7));
            WorldFlagSplits.Add(new WorldFlagSplitSetting("Yhorm the Giant", 0x5567, 7));
            WorldFlagSplits.Add(new WorldFlagSplitSetting("Dancer of the Boreal Valley", 0xF6C, 5));
            WorldFlagSplits.Add(new WorldFlagSplitSetting("Oceiros, the Consumed King", 0xF64, 1));
            WorldFlagSplits.Add(new WorldFlagSplitSetting("Ancient Wyvern", 0x2367, 7));
            WorldFlagSplits.Add(new WorldFlagSplitSetting("Dragonslayer Armour", 0x1467, 7));
            WorldFlagSplits.Add(new WorldFlagSplitSetting("Lothric, Younger Prince", 0x3764, 1));
            WorldFlagSplits.Add(new WorldFlagSplitSetting("Soul of Cinder", 0x5F67, 7));
            WorldFlagSplits.Add(new WorldFlagSplitSetting("Champion Gundyr", 0x5A64, 1));
            WorldFlagSplits.Add(new WorldFlagSplitSetting("Nameless King", 0x2369, 5));
            WorldFlagSplits.Add(new WorldFlagSplitSetting("Old Demon King", 0x5064, 1));
        }
        public void Load(XmlNode node) {
            foreach (var split in WorldFlagSplits) {
                var setting = node.SelectSingleNode($"//WorldFlag[@name='{split.Name}']");
                if (setting == null)
                    continue;
                split.Active = Boolean.Parse(setting.Attributes["active"].Value);
            }

            if(SettingsUpdated != null)
                SettingsUpdated(this, EventArgs.Empty);
        }

        public XmlNode Save(XmlDocument document) {
            var element = document.CreateElement("AutoSplitterSettings");
            foreach (var split in WorldFlagSplits) {
                if (!split.Active) {
                    element.AppendChild(CreateWorldFlagSetting(split, document));
                }
            }

            return element;
        }

        private XmlNode CreateWorldFlagSetting(WorldFlagSplitSetting split, XmlDocument document) {
            var setting = document.CreateElement("WorldFlag");
            var nameAttribute = document.CreateAttribute("name");
            var activeAttribute = document.CreateAttribute("active");

            nameAttribute.Value = split.Name;
            activeAttribute.Value = split.Active.ToString();

            setting.Attributes.Append(nameAttribute);
            setting.Attributes.Append(activeAttribute);

            return setting;
        }

        public List<WorldFlagSplitSetting> WorldFlagSplits { get; set; }

        public void UpdateSplits() {
            liveSplitState.Run.Clear();
            foreach (var split in WorldFlagSplits) {
                liveSplitState.Run.Add(new Segment(split.Name));
            }
        }
    }
}
