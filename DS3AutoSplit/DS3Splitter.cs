﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using DS3AutoSplit.Configs;
using DS3AutoSplit.Settings;
using DS3AutoSplit.Splits;
using LiveSplit.Model;
using Memory;

namespace DS3AutoSplit {
    public class Ds3Splitter : IDisposable {
        private readonly Ds3Settings settings;
        private readonly LiveSplitState liveSplitState;
        private Process process;
        private ProcessMemory processMemory;
        private IGameConfig config;
        private readonly TimerModel timer = new TimerModel();

        private IntPtr gameTimeBaseAddress;
        private long currentGameTime;
        private readonly List<ISplit> splits = new List<ISplit>();

        private DateTime lastUpdate = DateTime.Now;
        private IntPtr characterInfoBase;
        private string characterName;
        private string gameName = "Dark Souls III";

        public Ds3Splitter(Ds3Settings settings, LiveSplitState liveSplitState) {
            this.settings = settings;
            this.liveSplitState = liveSplitState;
            timer.CurrentState = liveSplitState;

            liveSplitState.OnStart += (sender, args) => {
                liveSplitState.IsGameTimePaused = true; // do not use LiveSplit GameTime
            };

            liveSplitState.OnReset += (sender, args) => {
                Reset();
            };

            liveSplitState.OnUndoSplit += (sender, args) => {
                splits.OrderByDescending(s => s.GameTimeCompleted).First().UndoSplit();
            };
        }


        public void Update() {
            if (DateTime.Now - lastUpdate < TimeSpan.FromMilliseconds(40))
                return;

            lastUpdate = DateTime.Now;

            if (process == null) {
                process = GetGameProcess();

                if (process == null)
                    return;

                Initialize();
            }

            if (gameTimeBaseAddress == IntPtr.Zero) {
                InitializeAddresses();
            }

            if (!UpdateGameTime()) {
                return; // Time is not running, so do not Update splits
            }

            //UpdateGameName();
            UpdateSplits();
        }

        private void UpdateGameName() {
            if (!string.IsNullOrEmpty(characterName)) {
                var nameAddress = processMemory.ReadIntPtr(characterInfoBase + 0x10);
                Trace.WriteLine($"nameAddress: {(long)nameAddress:X2}");

                characterName = processMemory.ReadUnicodeString(nameAddress + 0x88, 16).Trim();
                Trace.WriteLine($"Name: {characterName}");

                if (string.IsNullOrEmpty(characterName)) {
                    liveSplitState.Run.GameName = $"{gameName}";
                }

                liveSplitState.Run.GameName = $"{gameName} - ({characterName})";
            }
        }

        private void UpdateSplits() {
            foreach (var split in splits) {
                if (split.Split(currentGameTime, processMemory)) {
                    timer.Split();
                }
            }
        }

        private bool UpdateGameTime() {
            var currentMilliseconds = processMemory.ReadInt64(gameTimeBaseAddress);

            if (currentMilliseconds == 0) {
                return false;
            }

            if (liveSplitState.CurrentPhase == TimerPhase.NotRunning) {
                Trace.WriteLine($"Triggering AutoStart at {currentMilliseconds} ms");
                AutoStart();
                return false;
            }

            liveSplitState.SetGameTime(TimeSpan.FromMilliseconds(currentMilliseconds));
            currentGameTime = currentMilliseconds;

            return currentMilliseconds != 0;
        }

        private void Initialize() {
            config = null;
            
            processMemory = ProcessMemory.OpenRead(process.Id);
            var version = processMemory.GetFileVersion();
            Trace.WriteLine($"Game Version: {version}");

            switch (version) {
                case "1.12.0.0":
                    config = new GameConfigV112();
                    break;
                default:
                case "1.14.0.0":
                case "1.15.0.0":
                    config = new GameConfigV115();
                    break;
            }

            InitializeAddresses();
        }

        private void InitializeAddresses() {
            var baseModule = processMemory.GetMainModule();

            characterInfoBase = baseModule.ReadIntPtr(config.GameTimeAddress);
            gameTimeBaseAddress = characterInfoBase + config.GameTimeOffset;
            Trace.WriteLine($"gameTimeBaseAddress: {(long)gameTimeBaseAddress:X2}");

            var worldFlagBaseAddress = baseModule.ReadIntPtr(config.WorldFlagAddress);
            Trace.WriteLine($"WorldFlag Address: {(long)worldFlagBaseAddress:X2}");

            worldFlagBaseAddress = processMemory.ReadIntPtr(worldFlagBaseAddress);
            Trace.WriteLine($"WorldFlag Address: {(long)worldFlagBaseAddress:X2}");

            splits.Clear();

            foreach (var splitSetting in settings.WorldFlagSplits) {
                splits.Add(new WorldFlagSplit(splitSetting, worldFlagBaseAddress));
            }
        }

        private Process GetGameProcess() {
            process?.Dispose();
            processMemory?.Dispose();

            var processes = Process.GetProcessesByName("DarkSoulsIII");

            if (processes.Length == 0)
                return null;

            process = processes.First();
            process.EnableRaisingEvents = true;
            process.Exited += (sender, args) => Reset();

            return process;
        }

        private void AutoStart() {
            timer.Start();
        }

        private void Reset() {
            characterName = null;
            currentGameTime = 0;
            process?.Dispose();
            process = null;
            processMemory?.Dispose();
            processMemory = null;
        }

        public void Dispose() {
            process?.Dispose();
            processMemory?.Dispose();
        }
    }
}
