﻿namespace DS3AutoSplit.Settings {
    public class WorldFlagSplitSetting : ISplitSetting {
        public WorldFlagSplitSetting(string name, int offset, int flag) {
            Name = name;
            Active = true;
            Offset = offset;
            Flag = flag;
        }

        public string Name { get; }
        public bool Active { get; set; }
        public int Offset { get; }
        public int Flag { get; }
    }
}