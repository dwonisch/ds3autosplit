﻿
namespace DS3AutoSplit.Settings {
    partial class Ds3SettingsControl {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.flagsTree = new System.Windows.Forms.TreeView();
            this.btnCreateSplits = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // flagsTree
            // 
            this.flagsTree.CheckBoxes = true;
            this.flagsTree.Location = new System.Drawing.Point(0, 52);
            this.flagsTree.Name = "flagsTree";
            this.flagsTree.Size = new System.Drawing.Size(464, 474);
            this.flagsTree.TabIndex = 0;
            this.flagsTree.AfterCheck += new System.Windows.Forms.TreeViewEventHandler(this.flagsTree_AfterCheck);
            // 
            // btnCreateSplits
            // 
            this.btnCreateSplits.Location = new System.Drawing.Point(4, 4);
            this.btnCreateSplits.Name = "btnCreateSplits";
            this.btnCreateSplits.Size = new System.Drawing.Size(75, 23);
            this.btnCreateSplits.TabIndex = 1;
            this.btnCreateSplits.Text = "Create Splits";
            this.btnCreateSplits.UseVisualStyleBackColor = true;
            this.btnCreateSplits.Click += new System.EventHandler(this.btnCreateSplits_Click);
            // 
            // Ds3SettingsControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.btnCreateSplits);
            this.Controls.Add(this.flagsTree);
            this.Name = "Ds3SettingsControl";
            this.Size = new System.Drawing.Size(464, 526);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TreeView flagsTree;
        private System.Windows.Forms.Button btnCreateSplits;
    }
}
