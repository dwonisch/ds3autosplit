﻿using System.Windows.Forms;

namespace DS3AutoSplit.Settings {
    public partial class Ds3SettingsControl : UserControl {
        private readonly Ds3Settings settings;
        private TreeNode bossNode;

        public Ds3SettingsControl(Ds3Settings settings) {
            this.settings = settings;
            settings.SettingsUpdated += Settings_SettingsUpdated;
            InitializeComponent();
        }

        private void Settings_SettingsUpdated(object sender, System.EventArgs e) {
            LoadData();
        }

        private void LoadData() {
            bossNode = flagsTree.Nodes.Add("Bosses", "Bosses");

            foreach (var split in settings.WorldFlagSplits) {
                var treeNode = bossNode.Nodes.Add(split.Name, split.Name);
                treeNode.Checked = split.Active;

                treeNode.Tag = split;
            }
        }

        private void flagsTree_AfterCheck(object sender, TreeViewEventArgs e) {
            if (e.Node.Tag == null)
                return;

            ((ISplitSetting)e.Node.Tag).Active = e.Node.Checked;
        }

        private void btnCreateSplits_Click(object sender, System.EventArgs e) {
            settings.UpdateSplits();
        }
    }
}
