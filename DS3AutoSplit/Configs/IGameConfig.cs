﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DS3AutoSplit.Configs {
    interface IGameConfig {
        IntPtr GameTimeAddress { get; }
        int GameTimeOffset { get; }
        IntPtr WorldFlagAddress { get; }
    }
}
