﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DS3AutoSplit.Settings {
    public interface ISplitSetting {
        bool Active { get; set; }
    }
}
