﻿using System;

namespace DS3AutoSplit.Configs {
    public class GameConfigV113 : IGameConfig {
        public IntPtr GameTimeAddress => new IntPtr(0x473E018);
        public int GameTimeOffset => 0xA4;
        public IntPtr WorldFlagAddress => new IntPtr(0x473BE28);
    }
}