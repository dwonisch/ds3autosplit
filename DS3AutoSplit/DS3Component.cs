﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;
using System.Xml;
using DS3AutoSplit.Settings;
using LiveSplit.Model;
using LiveSplit.UI;
using LiveSplit.UI.Components;

namespace DS3AutoSplit {
    public class Ds3Component : IComponent {
        private readonly Ds3SettingsControl control;
        private readonly Ds3Settings settings;
        private readonly Ds3Splitter splitter;

        public Ds3Component(LiveSplitState liveSplitState) {
            settings = new Ds3Settings(liveSplitState);
            splitter = new Ds3Splitter(settings, liveSplitState);
            control = new Ds3SettingsControl(settings);
        }

        public void Dispose() {
            control?.Dispose();
        }

        public void DrawHorizontal(Graphics g, LiveSplitState state, float height, Region clipRegion) {
        }

        public void DrawVertical(Graphics g, LiveSplitState state, float width, Region clipRegion) {
        }

        public Control GetSettingsControl(LayoutMode mode) {
            return control;
        }

        public XmlNode GetSettings(XmlDocument document) {
            return settings.Save(document);
        }

        public void SetSettings(XmlNode settings) {
            this.settings.Load(settings);
        }

        public void Update(IInvalidator invalidator, LiveSplitState state, float width, float height, LayoutMode mode) {
            splitter.Update();
        }

        public string ComponentName => "Dark Souls III - Auto Split Component";
        public float HorizontalWidth => 0;
        public float MinimumHeight => 0;
        public float VerticalHeight => 0;
        public float MinimumWidth => 0;
        public float PaddingTop => 0;
        public float PaddingBottom => 0;
        public float PaddingLeft => 0;
        public float PaddingRight => 0;
        public IDictionary<string, Action> ContextMenuControls => null;
    }
}
