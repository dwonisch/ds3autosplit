﻿using System;

namespace DS3AutoSplit.Configs {
    public class GameConfigV112 : IGameConfig {
        public IntPtr GameTimeAddress => new IntPtr(0x473A818);
        public int GameTimeOffset => 0xA4;
        public IntPtr WorldFlagAddress => new IntPtr(0x47364C8);
    }
}